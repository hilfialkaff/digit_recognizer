import csv as csv
import numpy as np
from sklearn import neighbors

DATA_DIR = "./data"
TRAIN_DATA = DATA_DIR + "/train.csv"
TEST_DATA = DATA_DIR + "/test.csv"
OUTPUT = "./predict"

knn = neighbors.KNeighborsClassifier(n_neighbors=10)

def train():
    reader = csv.reader(open(TRAIN_DATA, 'rb'))
    train_data = []
    train_answer = []

    reader.next()
    for row in reader:
        train_answer.append(row[0])
        train_data.append(row[1:])

    train_answer = np.array(train_answer)
    train_data = np.array(train_data)
    knn.fit(train_data, train_answer)

def test():
    reader = csv.reader(open(TEST_DATA, 'rb'))
    test_data = []
    out = open(OUTPUT, 'w')
    i = 1

    reader.next()
    for row in reader:
        test_data.append(row)

    test_data = np.array(test_data)
    predict = knn.predict(test_data)

    out.write("ImageId,Label\n")
    for p in predict:
        out.write(str(i) + ',' + p + '\n')

if __name__ == '__main__':
    train()
    test()
